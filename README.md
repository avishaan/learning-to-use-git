# This is my README

The following file is meant to be used to learn both: how to use git with bitbucket and how to work together as a team
for making changes to a piece of software.

First, everyone needs to accept the invitation to bitbucket. Setup your git global settings with your Name and email
address if you have not done so already.

Everyone will start off by making a local repository. This local repository is where you will work on your own changes
before uploading it back to bitbucket for merging.

After you have initialized your own repository, you will need to clone the remote repository from
"https://bitbucket.org/avishaan/learning-to-use-git.git" This is your remote origin that you are adding from.
You, typically, will only clone a repository once.

Now that you have cloned the respository, you have a working copy that you can edit.

Open the 'GroupTest.js' locally and
make updates to the file as per the instruction I have given below. Everyone will be making their own changes to the
'MathObj' object I have already started in the file. This means when you are done with the changes, each of you will
have your own different file. This 'MathObj' will be a constructor.

Atul: You will add a method so the object can add 3 numbers together and return the result.
    example usage:  customMath = new MathObj();
            console.log(customMath.add(1,5,7)); // output is 13

Priyanka: You will add a method so the object can subtract the first number from the second.
    example usage:  customMath = new MathObj();
            console.log(customMath.subtract(5,1)); // output is 4

Rohit: You will add a method so the object can square a given number.
    example usage: customMath - new MathObj();
            console.log(customMath.square(3)); // output is 9


After each of your has finished his own addition to the 'MathObj', you will then stage and commit your changes locally.

Once you have made your changes and completed updating our 'MathObj', you will push your changes to the remote repository.
If you have trouble pushing the changes, make sure you have signed up for Bitbucket as per your email invitation and
make sure you have your git email setup as your codehatcher email. The public URL for the bitbucket share is
https://bitbucket.org/avishaan/learning-to-use-git.git where you can go if you need more help

At this point I will check the work of everyone.

T